public class Main {
    public static void main(String[] args) {
        int size = 15;
        for (int j = 1; j <= size; j++) {
            for (int i = 1; i <= size; i++) {
                if ((i * j) < 100 && (i*j) >=10) {
                    System.out.print(" " + (i * j) + "| ");
                }
                else if ((i*j)<10) {
                    System.out.print("  " + (i * j) + "| ");
                } else {
                    System.out.print((i * j) + "| ");
                }
            }
            System.out.println();

        }
    }
}
